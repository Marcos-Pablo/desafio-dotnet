﻿using Microsoft.EntityFrameworkCore;
using Domain.Models;

namespace Repository
{
    public class DataContext : DbContext
    {
        public DataContext() { }
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }
        public DbSet<User> Users { get; set; }
        public DbSet<Movie> Movies{ get; set; }
        public DbSet<Rating> Ratings{ get; set; }

        public async void teste()
        {
            await this.Users.ToListAsync<User>();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Rating>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.MovieId});
                entity.Property("Id").UseIdentityColumn(1, 1);
            });

            modelBuilder.Entity<User>()
                .HasIndex(u => u.Email)
                .IsUnique();
        }
    }
}
