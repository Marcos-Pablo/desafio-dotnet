﻿using Domain.Models;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace Repository.Repositories
{
    public class RatingRepository : BaseRepository<Rating, DataContext>
    {
        public RatingRepository(DataContext context) : base(context) { }

        public async Task<bool> Evaluate(Rating userRate)
        {
            Rating rate = await Get(userRate.UserId, userRate.MovieId);

            if (rate == null)
            {
                context.Ratings.Add(userRate);
                await context.SaveChangesAsync();
                return true;
            }
            return false;
        }

        public async Task<Rating> Get(int userId, int movieId)
        {
            Rating rate = await context.Ratings
                        .Where<Rating>(r => (r.UserId == userId && r.MovieId == movieId))
                        .FirstOrDefaultAsync<Rating>();
            return rate;
        }
    }
}
