﻿using Domain.Models;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using Domain.Dto;

namespace Repository.Repositories
{
    public class MovieRepository : BaseRepository<Movie, DataContext>
    {
        public MovieRepository(DataContext context) : base(context) { }

        public async Task<dynamic> GetMovieWithAverageRate(int movieId)
        {
            Movie movie = await context.Movies.Where<Movie>(movie => movie.Id == movieId)
                    .AsNoTracking<Movie>()
                    .FirstOrDefaultAsync<Movie>();

            var rating = await CalculateAverage(movieId);

            return new
            {
                movie,
                rating
            };
        }

        //public PagedList<Movie> GetAll(PaginationDto pagination)
        //{
        //    return PagedList<Movie>.ToPagedList(context.Movies.OrderBy(on => on.Name),
        //        pagination.PageNumber,
        //        pagination.PageSize);
        //}

        public List<Movie> GetAll(PaginationDto pagination, MovieParamatersDto movieParamaters)
        {
            var movies = context.Movies.AsQueryable().OrderBy(m => m.Name);
            if (movieParamaters.Genre != null) movies = movies.Where(m => m.Genre == movieParamaters.Genre).OrderBy(m => m.Name); 
            if (movieParamaters.Director != null) movies = movies.Where(m => m.Director == movieParamaters.Director).OrderBy(m => m.Name); 
            if (movieParamaters.Name != null) movies = movies.Where(m => m.Name == movieParamaters.Name).OrderBy(m => m.Name); 
            if (movieParamaters.Actor != null) movies = movies.Where(m => m.Actor == movieParamaters.Actor).OrderBy(m => m.Name); 
            movies = movies.Skip((pagination.PageNumber - 1) * pagination.PageSize)
                .Take(pagination.PageSize).OrderBy(m => m.Name);

            return movies.ToList();
        }

        private async Task<dynamic> CalculateAverage(int movieId)
        {
            List<Rating> ratings = await context.Ratings.Where(rating => rating.MovieId == movieId)
                .ToListAsync();

            if (ratings == null)
                return new
                {
                    average = 0,
                    numberOfVotes = 0
                };

            int sum = 0;
            foreach (Rating rate in ratings)
                sum += rate.Rate;

            double average = (sum * 1.0) / ratings.Count();

            return new
            {
                average = average,
                numberOfVotes = ratings.Count()
            };
        }

    }
}
