﻿using Domain.Models;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using Domain.Dto;

namespace Repository.Repositories
{
    public class UserRepository : BaseRepository<User, DataContext>
    {
        public UserRepository(DataContext context) : base(context) { }

        public async Task<User> Get(string email)
        {
            User user = await context.Users.Where(x => x.Email.ToLower() == email.ToLower()).FirstOrDefaultAsync();

            return user;
        }

        public PagedList<User> ListActiveUsers(PaginationDto pagination)
        {
            var itens = context.Users
                    .Where<User>(user => user.Active).OrderBy(on => on.Name);

            return PagedList<User>
                    .ToPagedList(itens,
                    pagination.PageNumber,
                    pagination.PageSize);
        }

        public async override Task<bool> Delete(int id)
        {
            User user = await Get(id);
            user.Deactivate();
            await context.SaveChangesAsync();
            return true;
        }

        public async Task<bool> DeleteUser(User user)
        {
            user.Deactivate();
            await context.SaveChangesAsync();
            return true;
        }

        public async Task<bool> Update(int userId, User user)
        {
            context.Entry(await context.Users.FirstOrDefaultAsync(x => x.Id == userId)).CurrentValues.SetValues( new {user.Email, user.Name, user.Password, user.Role });
            await context.SaveChangesAsync();
            return true;
        }
    }
}
