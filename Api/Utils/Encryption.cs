﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Api.Utils
{
    public class Encryption
    {
        private HashAlgorithm _algorithm;
        public Encryption(HashAlgorithm algorithm)
        {
            _algorithm = algorithm;
        }

        public string EncryptPassword(string password)
        {
            var encodedValue = Encoding.UTF8.GetBytes(password);
            var encryptedPassword = _algorithm.ComputeHash(encodedValue);

            var sb = new StringBuilder();
            foreach (var caracter in encryptedPassword)
            {
                sb.Append(caracter.ToString("X2"));
            }

            return sb.ToString();
        }

        public bool CheckPassword(string typedPassword, string registeredPassword)
        {
            if (string.IsNullOrEmpty(registeredPassword))
                throw new NullReferenceException("Cadastre uma senha.");

            var encryptedPassword = _algorithm.ComputeHash(Encoding.UTF8.GetBytes(typedPassword));

            var sb = new StringBuilder();
            foreach (var caractere in encryptedPassword)
            {
                sb.Append(caractere.ToString("X2"));
            }

            return sb.ToString() == registeredPassword;
        }
    }
}
