﻿using Domain.Models;
using Domain.Dto;
using Repository.Repositories;
using Api.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Api.Utils;
using System.Security.Cryptography;

namespace Api.Controllers
{
    [ApiController]
    [Route("/authentication")]
    public class AuthenticateController : ControllerBase    {

        private UserRepository _userRepository { get; set; }
        public AuthenticateController(UserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        [HttpPost]
        [Route("login")]
        public async Task<ActionResult<dynamic>> Authenticate(
            [FromBody] LoginDto login)
        {
            try
            {

                var user = await _userRepository.Get(login.Email);
                if (user == null)
                    return NotFound(new { message = "Email inválido!" });
                bool correctPassword = new Encryption(SHA512.Create()).CheckPassword(login.Password, user.Password);
                if (!correctPassword)
                    return NotFound(new { message = "Senha inválida!" });

                var token = TokenService.GenerateToken(user);

                user.Password = "";

                return Ok(new { user = user, token = token });
            }
            catch (Exception)
            {
                
                return StatusCode(StatusCodes.Status500InternalServerError, "Erro interno de servidor");
            }
        }

        [HttpPost]
        [Route("edit-profile")]
        [Authorize]
        public async Task<IActionResult> EditarPerfil(
            [FromBody] User data)
        {
            try
            {
                User user = await _userRepository.Get(User.Identity.Name);
                if(user == null)
                    return StatusCode(StatusCodes.Status403Forbidden, "É necesário fazer login novamente!");

                data.Password = new Encryption(SHA512.Create()).EncryptPassword(data.Password);
                await _userRepository.Update(user.Id, data);
                return Ok("Perfil editado com sucesso!");

            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Erro interno de servidor");
            }
        }
    }
}
