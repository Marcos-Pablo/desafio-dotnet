﻿using Repository;
using Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Repository.Repositories;
using Domain.Dto;
using Newtonsoft.Json;

namespace Api.Controllers
{
    [ApiController]
    [Route("/movies")]
    public class MovieController : ControllerBase
    {
        [HttpGet]
        [Route("")]
        [Authorize]
        public IActionResult ListMovies(
            [FromServices] MovieRepository movieRepository,
            [FromQuery] PaginationDto pagination,
            [FromQuery] MovieParamatersDto movieParamaters)
        {
            try
            {
                var movies = movieRepository.GetAll(pagination, movieParamaters);

                return Ok(movies);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Erro interno de servidor");
            }
        }
        [HttpGet]
        [Route("{MovieId}")]
        [Authorize]
        public async Task<dynamic> GetById(
            [FromServices] MovieRepository movieRepository,
            [FromRoute] int movieId)
        {
            try
            {
                var movie = await movieRepository.GetMovieWithAverageRate(movieId);
                if(movie != null)
                    return Ok(movie);

                return NotFound("Filme não encontrado!");
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Erro interno de servidor");
            }
        }

        [HttpPost]
        [Route("")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> RegisterMovie(
            [FromServices] MovieRepository movieRepository,
            [FromBody] Movie data)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Movie movie = await movieRepository.Add(data);
                    return Ok(movie);
                }
                else
                    return BadRequest(ModelState);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Erro interno de servidor");
            }
        }

        [HttpPut]
        [Route("{MovieId}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> UpdateMovie(
            [FromServices] MovieRepository movieRepository,
            [FromRoute] int movieId,
            [FromBody] Movie movie)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    movie.Id = movieId;
                    await movieRepository.Update(movie);
                    return Ok("Filme editado sucesso!");
                }
                else
                    return BadRequest(ModelState);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Erro interno de servidor");
            }
        }

        [HttpDelete]
        [Route("{MovieId}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> DeleteMovie(
            [FromServices] MovieRepository movieRepository,
            [FromRoute] int movieId)
        {
            try
            {
                Movie movie = await movieRepository.Get(movieId);
                if (movie == null)
                    return NotFound("Filme não encontrado!");
                await movieRepository.Delete(movieId);

                return Ok("Filme excluído com sucesso!");
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Erro interno de servidor");
            }
        }

        [HttpPost]
        [Route("evaluate")]
        [Authorize]
        public async Task<IActionResult> Evaluate(
            [FromServices] UserRepository userRepository,
            [FromServices] RatingRepository ratingRepository,
            [FromBody] Rating userRate)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);
                User user = await userRepository.Get(User.Identity.Name);
                userRate.UserId = user.Id;
                    
                bool rated = await ratingRepository.Evaluate(userRate);
                if (rated)
                {
                    return Ok("Filme avaliado com sucesso!");
                }
                return BadRequest("Filme já avaliado!");
            }
            catch (System.Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Erro interno de servidor");
            }
        }
    }
}
