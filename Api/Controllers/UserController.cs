﻿using Repository.Repositories;
using Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Api.Utils;
using System.Security.Cryptography;
using Domain.Dto;
using Newtonsoft.Json;

namespace Api.Controllers
{
    [ApiController]
    [Route("/users")]
    public class UserController : ControllerBase
    {
        [HttpGet]
        [Route("")]
        [Authorize(Roles = "admin")]
        public IActionResult ListActiveUsers(
            [FromServices] UserRepository userRepository,
            [FromQuery] PaginationDto pagination)
        {
            try
            {
                var users = userRepository.ListActiveUsers(pagination);
                var metadata = new
                {
                    users.TotalCount,
                    users.PageSize,
                    users.CurrentPage,
                    users.TotalPages,
                    users.HasNext,
                    users.HasPrevious
                };
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));

                return Ok(users);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Erro interno de servidor");
            }
        }
        [HttpGet]
        [Route("{UserId}")]
        [Authorize]
        public async Task<IActionResult> GetById(
            [FromServices] UserRepository userRepository,
            [FromRoute] int userId)
        {
            try
            {
                User user = await userRepository.Get(userId);

                if (user != null)
                    return Ok(user);
                return NotFound("Usuário não encontrado!");
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Erro interno de servidor");
            }
        }

        [HttpPost]
        [Route("")]
        public async Task<IActionResult> RegisterUser(
            [FromServices] UserRepository userRepository,
            [FromBody] User user)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    user.Password = new Encryption(SHA512.Create()).EncryptPassword(user.Password);
                    await userRepository.Add(user);
                    return Ok(user);
                }
                else
                    return BadRequest(ModelState);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Erro interno de servidor");
            }
        }

        [HttpPut]
        [Route("{UserId}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> UpdateUser(
            [FromServices] UserRepository userRepository,
            [FromRoute] int userId,
            [FromBody] User data)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    data.Id = userId;
                    data.Password = new Encryption(SHA512.Create()).EncryptPassword(data.Password);
                    await userRepository.Update(data);
                    return Ok("Usuário editado com sucesso!");
                }
                else
                    return BadRequest(ModelState);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Erro interno de servidor");
            }
        }

        [HttpDelete]
        [Route("{UserId}")]
        [Authorize]
        public async Task<IActionResult> DeleteUser(
            [FromServices] UserRepository userRepository,
            [FromRoute] int userId)
        {
            try
            {
                User user = await userRepository.Get(userId);
                if (user == null)
                    return NotFound("Usuário não encontrado!");
                await userRepository.Delete(userId);

                return Ok("Usuário excluído com sucesso!");
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Erro interno de servidor");
            }
        }
    }
}
