﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Dto
{
    public class MovieParamatersDto
    {
        public string Name { get; set; }
        public string Genre { get; set; }
        public string Director { get; set; }
        public string Actor { get; set; }

    }
}
