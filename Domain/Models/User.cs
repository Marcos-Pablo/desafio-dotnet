﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Models
{
    [Table(name: "User")]
    public class User : IEntity
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Este campo é obrigatório")]
        public string Name { get; set; }


        [Required(ErrorMessage = "Este campo é obrigatório")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Este campo é obrigatório")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Este campo é obrigatório")]
        [RegularExpression("admin|user", ErrorMessage = "Este campo precisa ser 'admin' ou 'user'")]
        public string Role { get; set; }

        public bool Active { get; set; } = true;


        public void Deactivate()
        {
            this.Active = false;
        }

    }
}
