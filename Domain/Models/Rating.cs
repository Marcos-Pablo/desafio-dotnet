﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Models
{
    [Table(name: "Rating")]
    public class Rating : IEntity
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Este campo é obrigatório")]
        [Range(0, 4, ErrorMessage = "A nota deve ser de 0 à 4")]
        public int Rate { get; set; }


        [Required(ErrorMessage = "Este campo é obrigatório")]
        public int MovieId { get; set; }
        public Movie Movie { get; set; }

        [Required(ErrorMessage = "Este campo é obrigatório")]
        public int UserId { get; set; }
        public User User { get; set; }
    }
}
