﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Models
{
    [Table(name: "Movie")]
    public class Movie : IEntity
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Este campo é obrigatório")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Este campo é obrigatório")]
        public string Genre { get; set; }

        [Required(ErrorMessage = "Este campo é obrigatório")]
        public string Director { get; set; }

        [Required(ErrorMessage = "Este campo é obrigatório")]
        public string Actor { get; set; }
    }
}
